# Battlefield V settings:

# VIDEO:
NOTE: start with advanced, as basic will reset after advanced settings are altered.
### Basic:
- Field of View: **105**
- Vehicle 3P field of view: **95**
- ADS Field of view: **Off**
- Motion Blur: **0%**
- ADS DOF Effects: **Off**
- Chromatic Aberration: **Off**
- Film Grain: **Off**
- Vignette: **Off**
- Lens Distortion: **Off**

### Advanced:
- DX12 Enabled: **On**
- High Dynamic Range: **Off**
- Framerate Limiter: **200**
- Future Frame Rendering: **Off**
- Vertical Sync: **Off**
- GPU Memory Restriction: **Off**
- Graphics Quality: **Custom**
- Texture Quality: **Low**
- Texture Filtering: **Low**
- Lighting Quality: **Low**
- Effects Quality: **Low**
- Post Process Quality: **Low**
- Mesh Quality: **Ultra**
- Terrain Quality: **Low**
- Undergrowth Quality: **Low**
- Antialiasing Post-Processing: **TAA Low** 
- Ambient Occlusion: **Off**
- High Fidelity Objects Amount: **Low**

# GAMEPLAY:
### Basic: 
- HUD Motion: **Off**

### Advanced: 
- Camera Shake Scale: **50%**
- Parachute Auto Deploy: **Off**
- Reload Hint: **Off**
- Network Performance stats: **show always**

# CONTROLS:
### Basic:
- Soldier Mouse Aim Sensitivity: **10%**

# KEY BINDINGS:
### Common: 
- Select 'All' chat channel: **U**
- Select 'Team' chat channel: **Y**
- VOIP: **V**
### Soldier:
- Danger Ping (Toggle) / Commo Rose (Hold): **Mouse3**
- Switch Seat in Vehicle: **X**
- Inspect Weapon: **T**
- Fire Mode / Scope Zeroing Distance: **'**
- Toggle Toolbox: **I**
- Prone: **CAPS LOCK**

