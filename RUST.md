# RUST SETTINGS:

LAUNCH OPTIONS:
```
-headlerp_inertia false  <== moves head back faster after alt-looking
```
AMD adrenalin settings: gaming: Rust: custom color: saturation: 160
USED COMMANDS:
```
graphics.vm_fov_scale false
input.sensitivity 0.415
client.lookatradius 0.07  (Default: 0.2; Min: 0.01; Max: 20) <== more accurate picking up of items, better at picking up when riding horses.
inventory.quickcraftdelay 0  <== Removes slight delay, when opening quick-craft.
bind f1 combatlog;consoletoggle  
bind f2 combatlog_outgoing;consoletoggle  <== Shows only what dmg u have done to someone else.
bind v forward;sprint <== autorun
bind c "~graphics.fov 60;graphics.fov 90" <== Toggle FOV for sniping.
bind backquote "craft.add -2072273936 1" <== Crafts bandages with backquote [tilde]
hitnotify.notification_level 2 <== Disables hitmarkers from invalids.
player.recoilcomp true <== Will make your weapon reposition to where it started after firing just 1 shot (great for m249 tapping)
```
# OPTIONS:
### Gameplay:
- Field of View: **90**
- Head bob: **Off**
- Crosshair: **On**
- Hit cross: **On**
- Hurt Flash: **Off**
- FPS Counter: **Advance + ping**
- Rich presence: **Off**
### Physics:
- Max Gibs: **0** (less particles when breaking/raiding walls)
- Creation Effects: **Off**

# AUDIO:
### Volume:
- Master volume: **0.5**
- Music volume: **0**
- Menu music volume: **0**
- Voices Volume: **4.0**
- Game sounds volume: **1**
- Instruments volume: **0.2**

# CONTROLS:
### Input settings:
- Mouse sensitivity: 0.415 **(NEED TO TYPE IN TERMINAL)**
- Always sprint: **Off**
### Binds:
- ping: **mouse2** (middle mouse button)
- Inventory toggle: **Q**
- Crafting toggle: **TAB**
- Voice transmit: **V** (don't use middle mouse button, as its used to split items)
- hover loot: **mouse3** (backwards mouse side button); **unbind h**
- examine held item: **T**
- gestures: **B**
- slot5: **X**

# SCREEN:
- Vsync: **Off**
- FPS Limit: **0**
- Limit FPS in background: **On**
- Limit FPS in menu: **On**

# GRAPHICS:
### Graphics quality:
- Graphics quality: **Potato** (Doesn't effect gameplay, can turn up higher)
- Shadow quality: **0**
- Shadow cascades: **No Cascades**
- Max Shadow Lights: **0**
- Water Quality: **0**
- Water Reflections: **0**
- Shader Level: **100**
- Draw Distance: **2500**
- Shadow distance: **50**
- Anisotropic Filtering: **1**
- Parallax Mapping: **0**
- Global Rendering: ???
- Global Render Distance: ???
- Grass Displacement: **On** (when on, easier to spot dropped guns.)
- Grass Shadows: **Off**
### Mesh quality: 
- Particle Quality: **25**
- Object Quality: **50**
- Tree Quality: **100**
- Max Tree Meshes: **100**
- Terrain Quality: **0**
- Grass Quality: **0**
- Decor Quality: **0**

# IMAGE EFFECTS:
- All Settings **Off**

# ACCESSIBILITY:
- Tree Marker colour: **Orange**
- Team Name colour: **Purple**

# EXPERIMENTAL: 
- All Settings **Off**
- Optimized Loading: **On** (if have SSD, supposedly helps load faster into server)

# RUST TIPS:
- It takes half the time to pickup a teammate with a bandage (6s --> 3s)
- When you equip horse armor it protects your entire body
- TC room layout:
![TC-room-layout](./Rust_TC_layout.webp)
