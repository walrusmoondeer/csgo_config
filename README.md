# Links to my Game Configs:
- [**Rust Config**](https://codeberg.org/kmelb/game_configs/src/branch/main/RUST.md)
- [**Apex Legends Config**](https://codeberg.org/kmelb/game_configs/src/branch/main/APEX_LEGENDS.md)
- [**Minecraft Config**](https://codeberg.org/kmelb/game_configs/src/branch/main/MINECRAFT.md)

# My CS2 Config:
[**autoexec.cfg**](https://codeberg.org/kmelb/game_configs/src/branch/main/Steam/steamapps/common/Counter-Strike%20Global%20Offensive/game/csgo/cfg/autoexec.cfg)   
[**prac.cfg**](https://codeberg.org/kmelb/game_configs/src/branch/main/Steam/steamapps/common/Counter-Strike%20Global%20Offensive/game/csgo/cfg/prac.cfg)   
[**useful_commands.cfg**](https://codeberg.org/kmelb/game_configs/src/branch/main/Steam/steamapps/common/Counter-Strike%20Global%20Offensive/game/csgo/cfg/useful_commands.cfg)   
### VIDEO SETTINGS:
- Resolution: **1152x864@240**
- Display Mode: **Fullscreen**
-----------------
- MSAA mode: **"4x"**
- Global Shadow Quality: **"High"** *High gives an advantage on maps with indoor lighting like 'Nuke', otherwise can use "Low"*
- Model / Texture detail: **"Medium"**
- Shader detail: **"Low"**
- Particle detail: **"Low"**
- Ambient Occlusion: **"Disabled"**
- Texture filtering mode: **"Bilinear"**
- High Dynamic Range: **"Performance"**
- FidelityFX super resolution: **"Disabled (highest quality)"** 

### CROSSHAIR V4  1152x864 22.05.2024
```
cl_crosshairstyle 4;
cl_crosshairsize 2.1;
cl_crosshairthickness 0.9;
cl_crosshairgap -2.3;
cl_crosshair_drawoutline 0;
cl_crosshairdot 0;
cl_crosshair_t 0;
cl_crosshairusealpha 1;
cl_crosshairalpha 230;
cl_crosshair_recoil 0;
cl_crosshairgap_useweaponvalue 0;
// RGB is necessary, bcs of bug;
cl_crosshaircolor_r 256;
cl_crosshaircolor_g 0;
cl_crosshaircolor_b 256;
//cl_crosshaircolor 5;
cl_crosshair_sniper_width 1;
cl_crosshair_friendly_warning 1;
hud_showtargetid 0;
```

### CROSSHAIR V3  1152x864 03.02.2024:
```
cl_crosshairstyle 4;
cl_crosshairsize 3;
cl_crosshairthickness 1;
cl_crosshairgap -3;
cl_crosshair_drawoutline 0;
cl_crosshairdot 0;
cl_crosshair_t 0;
cl_crosshairusealpha 1;
cl_crosshairalpha 192;
cl_crosshair_recoil 0;
cl_crosshairgap_useweaponvalue 0;
cl_crosshaircolor_r 256;
cl_crosshaircolor_g 0;
cl_crosshaircolor_b 256;
cl_crosshair_sniper_width 1;
cl_crosshair_friendly_warning 1;
hud_showtargetid 0;
```

### LAUNCH OPTIONS:
```
-nojoy -freq 240 -high 
```
```
+exec autoexec.cfg  <== use to apply config
-w 1280 -h 960 -noborder  <== specify resolution, and run game in fullscreen mode
-novid  <== novid doesnt work anymore
-allow_third_party_software <== use this to display 'msi afterburner overlay'.
```

### AUTOEXEC PATHs:
- LINUX PATH = **'$HOME/.local/share/Steam/steamapps/common/"Counter-Strike Global Offensive"/game/csgo/cfg/autoexec.cfg'**
- WINDOWS PATH = **'C:\Program Files (x86)\Steam\steamapps\common\Counter-Strike Global Offensive\game\csgo\cfg\autoexec.cfg'**
