# My Minecraft settings
### Settings:
- fabric mod loader (open-source)
- multimc5 (open-source)
- sensitivity: 46% (exact, calculated with a special calculator)

### Fabric Mods:
- **NOTE:** optifine is proprietary so better use Fabric (which is open-source), but not quite as feature-full as optifine.
- [**Sodium (Fabric)**](https://modrinth.com/mod/sodium) <-- optifine but on fabric mod loader
- [**Lithium (Fabric)**](https://modrinth.com/mod/lithium) <-- expands on Sodium, optifine but for fabric mod loader
- ~~Phosphorus~~ <-- more performant light engine for fabric (surpassed by starlight)
- [**Starlight (Fabric)**](https://modrinth.com/mod/starlight)  <-- EVEN more performant light engine for fabric (replacement for phosphorus)
- [Iris Shaders (Fabric)](https://modrinth.com/mod/iris) <-- open-source shader engine
- [Complementary Shaders - Reimagined](https://modrinth.com/shader/complementary-reimagined) <-- open-source shader
- [Entity culling (Fabric)](https://modrinth.com/mod/entityculling/version/d20sUcYn) <-- optimizing unseen blocks/entities
- [Faithful 32x (texture pack)](https://faithfulpack.net/downloads) <-- Default, but upgraded texture pack.
- [Fabric-API (Fabric)](https://modrinth.com/mod/fabric-api/version/0.89.0+1.20.1) <-- needed by Entity culling

### Minecraft installation:
```bash
paru -Ss jre17-openjdk multimc5
```

### Hacked clients (open-source):
- [Meteor](https://www.meteorclient.com) <-- open-source, available for newest version, can be installed as a mod on fabric
- [wurst](https://www.wurstclient.net) <-- mc client for linux (open-source, available for newest versions)
- Impact <-- mc client for linux (open-source), packaged with baritone, free, runs on linux, can be installed to work with multimc (not available for newest version) (recommended by FitMC)
    - installation process:
        - download 'Impact' jar file
        - java -jar <the impact jar file\>
        - in the installation menu select MultiMC 
- future <-- (open-source), used by fitmc usually, costs 19 eiro (not available for newest version) (recommended by FitMC)
- ~~Salhack~~
- ~~Kami Blue~~
