# My Apex Legends setting
### VIDEO SETTINGS:
- Resolution: **Native**
- Field of View (FOV): **110**
- FOV Ability Scaling: **Disabled**
- Sprint View Shake: **Minimal**
- V-Sync: **Disabled**
- Adaptive Resolution FPS Target: **0**
- Anti-aliasing: **None**
- Texture filtering: **Anisotropic 2x**
- Ambient Occlusion Quality: **Disabled**
- Sun Shadow Coverage: **Low**
- Sun Shadow Detail: **Low**
- Spot Shadow Detail: **Disabled**
- Volumetric Lighting: **Disabled**
- Dynamic Spot Shadows: **Disabled**
- Model Detail: **Low**
- Effects Detail: **Low**
- Impact Marks: **Disabled**
- Ragdolls: **Low**

### GAME SETTINGS: 
- Interact Prompt Style: **Compact** 
- Button Hints: **Off** 
- Crosshair Damage Feedback: **Off**
- Damage Numbers: **Stacking**
- Ping Opacity: **Faded**
- Obituaries: **On**
- Minimap Rotation: **On**
- Weapon Auto-Cycle on Empty: **On**
- Auto-Sprint: **On**
- Double-Tap Sprint: **Off** 
- Jetpack Control: **Hold** 
- Incoming Damage Feedback: **3D**
- Taking Damage Closes Deathbox or Crafting Menu: **Off**
- Hop-Up Pop-Up: **On**
- Performance Display: **On** 
- Reticle: **Customize(cyan crosshair)**

### MOUSE/KEYBOARD SETTINGS:
- Sensitivity: **2.12**
- Lighting Effects: **Off**
- Crouch (Toggle): **Clear**
- Tactical Ability: **Clear mouse button**
- Ultimate Ability: **Clear mouse button**
- Alternate Interact: ???
- Toggle Fire Mode: **'**
- Equip Survival item: **CAPS LOCK**
- Use Syringe: **X**
- Use Medkit: **6**
- Use Shield Cell: **C**
- Use Shield Battery: **ALT**
- Use Pheonix kit: **5**
- Inspect Weapon: **T**
- Ping: **Mouse 4**
- Push To Talk (Hold): **Middle Mouse Button**


### AUDIO SETTINGS:
- Master Volume: **50%**
- Incoming Voice Chat Volume: **100%**
- Sound Effects Volume: **100%** 
- Dialogue Volume: **50%** 
- Music Volume: **0%**
- Lobby Music Volume: **0%**
- Sound In Background: **On**
